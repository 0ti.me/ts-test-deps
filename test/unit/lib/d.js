const { d, expect, pquire, sinon, uuid } = deps;

const me = __filename;

/* using it here for visualization purposes */
d(me, 'with', 'more', 'args', () => {
  let callback;
  let filename;
  let mockDescribe;
  let mockDescribeResult;
  let configuredLibD;
  let mockPathRelative;
  let mockPathRelativeResult;
  let mocks;
  let testDirectory;

  beforeEach(() => {
    mocks = {};

    mockPathRelativeResult = `mock-path-relative-result-${uuid()}`;
    mockPathRelative = sinon.stub().returns(mockPathRelativeResult);
    mocks.path = {};
    mocks.path.relative = mockPathRelative;

    mockDescribeResult = `mock-describe-result-${uuid()}`;
    mockDescribe = sinon.stub().returns(mockDescribeResult);
    mocks.mocha = {};
    mocks.mocha.describe = mockDescribe;

    callback = `callback-${uuid()}`;
    filename = `filename-${uuid()}`;
    testDirectory = `test-directory-${uuid()}`;

    configuredLibD = pquire(
      me,
      mocks,
    )({
      describe: mockDescribe,
      utilities: { info: { directories: { test: testDirectory } } },
    });
  });

  it('should behave normally without any extra args', () => {
    expect(configuredLibD(filename, callback)).to.equal(mockDescribeResult);

    expect(mockPathRelative).to.have.been.calledOnceWithExactly(
      testDirectory,
      filename,
    );

    expect(mockDescribe).to.have.been.calledOnceWithExactly(
      mockPathRelativeResult,
      callback,
    );
  });

  it('should call describe with the expected arguments', () => {
    const args = new Array(4).fill(0).map((_, i) => `arg${i + 1}${uuid()}`);

    expect(configuredLibD(filename, ...args, callback)).to.equal(
      mockDescribeResult,
    );

    expect(mockPathRelative).to.have.been.calledOnceWithExactly(
      testDirectory,
      filename,
    );

    expect(mockDescribe).to.have.been.calledOnceWithExactly(
      `${mockPathRelativeResult} ${args[0]} ${args[1]} ${args[2]} ${args[3]}`,
      callback,
    );
  });
});
