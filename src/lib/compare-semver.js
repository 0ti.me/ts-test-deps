/**
 * A ascending sort function implementation of comparing semvers
 * Implemented to sort any patterns following schemes like these, sorted ascending for example:
 *   0.0.1
 *   0.0.11
 *   0.1.0
 *   1.0.0
 *   1.0.0-alpha (note: comparing with 1.0.0, they would be equal returning a 0)
 *   1.0.0 (note: this is the same as 1.0.0-alpha by this sort method)
 *   1.0.0-alpha1
 */
module.exports = (a, b) => {
  const semverRegex =
    /([0-9]+)\.([0-9]+)\.([0-9]+)(?:[-][a-zA-Z0-9-]*?)?([0-9]+)?$/;

  const aList = (semverRegex.exec(a) || [])
    .slice(1)
    .filter((x) => x !== undefined)
    .map((ea) => parseInt(ea));
  const bList = (semverRegex.exec(b) || [])
    .slice(1)
    .filter((x) => x !== undefined)
    .map((ea) => parseInt(ea));

  if (aList.length < 3) {
    throw new Error(`The first semver ${a} was unable to be parsed.`);
  }

  if (bList.length < 3) {
    throw new Error(`The second semver ${b} was unable to be parsed.`);
  }

  let len = aList.length > bList.length ? aList.length : bList.length;

  for (let i = 0; i < len; ++i) {
    if (aList.length <= i) {
      /* end of list and bList has more records, indicating b > a */
      return -1;
    }

    if (bList.length <= i) {
      /* end of list and aList has more records, indicating a > b */
      return 1;
    }

    if (aList[i] > bList[i]) {
      /* the first different number indicated a > b */
      return 1;
    } else if (bList[i] > aList[i]) {
      /* the first different number indicated b > a */
      return -1;
    }
  }

  return 0;
};
